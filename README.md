# install
```
pip install -r requirements.txt
```
# build
```
pip install pyinstaller
bash ./linux/build.sh
```
# run
```
./dist/redshift
```
# install linux
```
bash ./linux/install.sh
```