#!/bin/bash

echo -e "\n# Instalando Redshift...\n"

pacman -S redshift --noconfirm

dir_desktops=/usr/share/applications
dir_install=/opt/redshift

sudo rm -r $dir_install/
sudo mkdir $dir_install/

sudo cp ./dist/** $dir_install/
sudo chmod -R +r $dir_install/

desktop="[Desktop Entry]
Name=Redshift
Exec=$dir_install/redshift
Icon=$dir_install/redshift.png
Type=Application
Terminal=false
Categories=Utility;
Path=$dir_install/"

sudo rm -r $dir_desktops/redshift.desktop
echo "$desktop" | sudo tee $dir_desktops/redshift.desktop > /dev/null

echo -e "# Fim\n"