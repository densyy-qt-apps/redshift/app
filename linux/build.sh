#!/bin/bash

dir_img=./src/public/img
dir_layout=./src/layout

echo -e "\n# Configurando ambiente...\n"
rm -r ./dist
mkdir -p ./dist
cp $dir_img/redshift.png ./dist
pyuic5 $dir_layout/layout.ui -o $dir_layout/layout.py

echo -e "\n# Gerando executavel...\n"
pyinstaller ./src/main.py \
    --name redshift \
    --icon $dir_img/redshift.png \
    --onefile
chmod +x ./dist/redshift

echo -e "\n# Fim\n"