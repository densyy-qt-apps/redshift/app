import subprocess

#
# Constrollers
#

def night_mode(main_window):
    command = "redshift -P -O 4500K -g 0.8"
    run_shell(command)
    set_label_status(main_window, "Night ON")

def light_mode(main_window):
    command = "redshift -P -O 6500K -g 1"
    run_shell(command)
    set_label_status(main_window, "Light ON")

#
# System Methods
#

def run_shell(command):
    subprocess.call(command, shell=True)

#
# Forms Methods
#

def set_label_status(main_window, text):
    main_window.ui.label_status.setText(text)