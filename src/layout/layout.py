# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './src/layout/layout.ui'
#
# Created by: PyQt5 UI code generator 5.15.4
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(306, 278)
        MainWindow.setStyleSheet("background-color: rgb(24, 23, 24);")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.button_light_mode = QtWidgets.QPushButton(self.centralwidget)
        self.button_light_mode.setGeometry(QtCore.QRect(90, 190, 131, 28))
        self.button_light_mode.setStyleSheet("font: 12pt \"Roboto\";\n"
"color: rgb(230, 229, 231);\n"
"background-color: rgb(82, 161, 229);\n"
"border: 0;")
        self.button_light_mode.setObjectName("button_light_mode")
        self.button_night_mode = QtWidgets.QPushButton(self.centralwidget)
        self.button_night_mode.setGeometry(QtCore.QRect(90, 150, 131, 28))
        self.button_night_mode.setStyleSheet("font: 12pt \"Roboto\";\n"
"color: rgb(230, 229, 231);\n"
"background-color: rgb(229, 111, 142);\n"
"border: 0;")
        self.button_night_mode.setCheckable(False)
        self.button_night_mode.setObjectName("button_night_mode")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(90, 100, 57, 21))
        self.label.setStyleSheet("color: rgb(230, 229, 231);\n"
"font: 12pt \"Roboto\";")
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.label_status = QtWidgets.QLabel(self.centralwidget)
        self.label_status.setGeometry(QtCore.QRect(150, 100, 71, 21))
        self.label_status.setStyleSheet("color: rgb(230, 229, 231);\n"
"font: 12pt \"Roboto\";")
        self.label_status.setText("")
        self.label_status.setObjectName("label_status")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(0, 30, 301, 41))
        self.label_2.setStyleSheet("font: 20pt \"Roboto\";\n"
"color: rgb(230, 229, 231);")
        self.label_2.setAlignment(QtCore.Qt.AlignCenter)
        self.label_2.setObjectName("label_2")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 306, 25))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Redshift"))
        self.button_light_mode.setText(_translate("MainWindow", "Light"))
        self.button_night_mode.setText(_translate("MainWindow", "Night"))
        self.label.setText(_translate("MainWindow", "Mode:"))
        self.label_2.setText(_translate("MainWindow", "RedShift"))
