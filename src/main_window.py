from PyQt5.QtWidgets import QMainWindow
from layout.layout import Ui_MainWindow
from controllers import night_mode, light_mode

class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()

        # Instancia a classe gerada pela conversão do arquivo .ui
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        # Connect elements
        self.ui.button_night_mode.clicked.connect(lambda: night_mode(self))
        self.ui.button_light_mode.clicked.connect(lambda: light_mode(self))    
